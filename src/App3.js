import React from 'react';
import ReactDOM from 'react-dom';

function Square(props) {  

    let btnClass = (props.isAWinner ? 'winner' : 'square');

    return (
      <button 	
        className={btnClass}
        onClick={props.onClick}
      >
        {props.value}
      </button>
    );
}

class Board extends React.Component {  
  renderSquare(i) {
	  
	let isAWinner= false; 
    if(this.props.isWinner && (this.props.winnerLine[0] === i || this.props.winnerLine[1] === i || this.props.winnerLine[2] === i )){
		isAWinner = true;
	} 
     
   return( 
      <Square value= {this.props.squares[i]}
			  isAWinner= {isAWinner}
              onClick={ () => this.props.onClick(i) }        
      />
    );
  }

  render() {
    return (
      <div>
        <div className="board-row">
          {this.renderSquare(0)}
          {this.renderSquare(1)}
          {this.renderSquare(2)}
        </div>
        <div className="board-row">
          {this.renderSquare(3)}
          {this.renderSquare(4)}
          {this.renderSquare(5)}
        </div>
        <div className="board-row">
          {this.renderSquare(6)}
          {this.renderSquare(7)}
          {this.renderSquare(8)}
        </div>
      </div>
    );
  }
}

class Game extends React.Component {
  constructor(props){
   super(props);
   this.state={
     history: [{
        squares: Array(9).fill(null)
     }],
	 historyMoves: [{
        movesXY: [{
		   xy: Array(2).fill(null)
	   }]
     }],
     stepNumber: 0,
     xIsNext: true
   };
  }
  
  jumpTo(step){
    this.setState({
        stepNumber: step,
        xIsNext: (step%2) === 0
    });
  }
  
  handleClick(i) {
    const history  = this.state.history.slice(0, this.state.stepNumber + 1);
    const current  = history[history.length - 1];
    const squares  = current.squares.slice();
	
	const historyMoves  = this.state.historyMoves.slice(0, this.state.stepNumber + 1);
    const currentMove  = historyMoves[historyMoves.length - 1];
    const moves  = currentMove.movesXY.slice();
	
	let isWinner = CalculateWinner2(squares)['winner'];
    
	if( isWinner || squares[i]){
      return;
    } 
	
    squares[i] = this.state.xIsNext ? 'X' : 'O';
	
	let mutare_pas = [];
	if     (i===0) {  mutare_pas[0] = 1; mutare_pas[1] = 1; }
	else if(i===1) {  mutare_pas[0] = 1; mutare_pas[1] = 2; }
	else if(i===2) {  mutare_pas[0] = 1; mutare_pas[1] = 3; }
	else if(i===3) {  mutare_pas[0] = 2; mutare_pas[1] = 1; }
	else if(i===4) {  mutare_pas[0] = 2; mutare_pas[1] = 2; }
	else if(i===5) {  mutare_pas[0] = 2; mutare_pas[1] = 3; }
	else if(i===6) {  mutare_pas[0] = 3; mutare_pas[1] = 1; }
	else if(i===7) {  mutare_pas[0] = 3; mutare_pas[1] = 2; }
	else if(i===8) {  mutare_pas[0] = 3; mutare_pas[1] = 3; }	

	console.log("moveXY: "+this.state.historyMoves);
	console.log("just history: "+this.state.history);

    this.setState({
       history: history.concat([{squares: squares}]),
       stepNumber : history.length,
       xIsNext: !this.state.xIsNext
    });
  }
  
  render() {
    const history = this.state.history;
    const current = history[this.state.stepNumber];
    const winner = CalculateWinner2(current.squares)['winner'];
    
    const moves_squares = history.map((step, move) => {
          const desc = move ? "Go to move #" + move :  "Go to game start";		
		  return(
			<li key={move}>
			  <button onClick={() =>  this.jumpTo(move)}>{desc}</button>
			</li>
      );
    });	
          
    let status;
	let isWinner;
	let winnerLine = [];
	
	//console.log(CalculateWinner2(current.squares));
    
	if(winner){
      status = 'Winner is '+ winner;
	  isWinner = true;
	  
	  let line= CalculateWinner2(current.squares);
	  winnerLine = [line['a'], line['b'], line['c']];
	  
    } else {
      status = 'Next player is ' + (this.state.xIsNext ? 'X' : 'O') ;
	  isWinner = false;
    }
    
          
    return (
      <div className="game">
        <div className="game-board">
          <Board 
		    isWinner={isWinner}
			winnerLine={winnerLine}
            squares={current.squares}
            onClick={ i => this.handleClick(i)}
          />
        </div>
        <div className="game-info">
          <div style={{color: (isWinner ? "red" : "black")}}> {status} </div>
          <ol>{moves_squares}</ol>
        </div>
      </div>
    );
  }
}

// ========================================

ReactDOM.render(
  <Game />,
  document.getElementById('root')
);


function CalculateWinner2(squares){
  const Lines=[ /*combinatiile posibile de a castiga*/
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [2, 4, 6],
  ];
  
  let response = {}; 
  
  for(let i=0; i<Lines.length; i++ ){
    const[a, b, c] = Lines[i];
    if(squares[a] && squares[a] === squares[b] && squares[a] === squares[c]){
	  //console.log("a castigat"+a+","+b+","+c);	  
	  response = {...response, winner:squares[a], a:a, b:b, c:c}
	  return response;
    }
  }
  
  response = {...response, winner:null, a:null, b:null, c:null}
  return response;
}

export  default Game;